module com.ricardo.domain_crawler {
    requires javafx.controls;
    requires javafx.fxml;
	requires org.jsoup;
	requires com.google.common;
	requires java.persistence;
	requires org.hibernate.orm.core;
	requires java.sql;
	
    opens com.ricardo.domain_crawler.controller to javafx.fxml;
    opens com.ricardo.domain_crawler.entity to org.hibernate.orm.core;
    exports com.ricardo.domain_crawler;
}