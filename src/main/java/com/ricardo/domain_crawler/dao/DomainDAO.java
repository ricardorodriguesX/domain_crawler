package com.ricardo.domain_crawler.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

import com.ricardo.domain_crawler.entity.Domain;


public interface DomainDAO extends Serializable{
	void create(Domain domain);

    void destroy(int id) throws EntityNotFoundException;

    void edit(Domain domain) throws EntityNotFoundException, Exception;

    Domain findDomain(int id);

    List<Domain> findDomainEntities();

    List<Domain> findDomainEntities(int maxResults, int firstResult);

    EntityManager getEntityManager();

    int getDomainCount();
}
