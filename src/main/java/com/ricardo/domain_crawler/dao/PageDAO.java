package com.ricardo.domain_crawler.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

import com.ricardo.domain_crawler.entity.Page;


public interface PageDAO  extends Serializable{
	void create(Page page);

    void destroy(int id) throws EntityNotFoundException;

    void edit(Page page) throws EntityNotFoundException, Exception;

    Page findPage(int id);

    List<Page> findPageEntities();

    List<Page> findPageEntities(int maxResults, int firstResult);

    EntityManager getEntityManager();

    int getPageCount();
}
