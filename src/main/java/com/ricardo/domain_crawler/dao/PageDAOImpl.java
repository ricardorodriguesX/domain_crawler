package com.ricardo.domain_crawler.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.ricardo.domain_crawler.entity.Page;

public class PageDAOImpl implements PageDAO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2863394655821049664L;
	// ATRIBUTOS
	private EntityManagerFactory emf = null;

	public PageDAOImpl(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public void create(Page page) {
		EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(page);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
	}

	@Override
	public void destroy(int id) throws EntityNotFoundException {
		EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Page page;
            try {
            	page = em.getReference(Page.class, id);
            	page.getId();
            } catch (EntityNotFoundException enfe) {
                throw new EntityNotFoundException("The page with id " + id + " no longer exists.");
            }
            em.remove(page);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }

	}

	@Override
	public void edit(Page page) throws EntityNotFoundException, Exception {
		EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            page = em.merge(page);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = page.getId();
                if (findPage(id) == null) {
                    throw new EntityNotFoundException("The page with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }

	}

	@Override
	public Page findPage(int id) {
		EntityManager em = getEntityManager();
        try {
            return em.find(Page.class, id);
        } finally {
            em.close();
        }
	}

	@Override
	public List<Page> findPageEntities() {
		return findPageEntities(true, -1, -1);
	}

	@Override
	public List<Page> findPageEntities(int maxResults, int firstResult) {
		return findPageEntities(false, maxResults, firstResult);
	}
	
	private List<Page> findPageEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Page.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
	
	@Override
	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	@Override
	public int getPageCount() {
		EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Page> rt = cq.from(Page.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
	}

}
