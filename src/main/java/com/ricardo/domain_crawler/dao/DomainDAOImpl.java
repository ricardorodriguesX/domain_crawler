package com.ricardo.domain_crawler.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.ricardo.domain_crawler.entity.Domain;

public class DomainDAOImpl implements DomainDAO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4558249069128092450L;
	// ATRIBUTOS
	private EntityManagerFactory emf = null;

	public DomainDAOImpl(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	@Override
	public void create(Domain domain) {
		EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(domain);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
	}

	@Override
	public void destroy(int id) throws EntityNotFoundException {
		EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Domain domain;
            try {
            	domain = em.getReference(Domain.class, id);
            	domain.getId();
            } catch (EntityNotFoundException enfe) {
                throw new EntityNotFoundException("The domain with id " + id + " no longer exists.");
            }
            em.remove(domain);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
	}

	@Override
	public void edit(Domain domain) throws EntityNotFoundException, Exception {
		EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            domain = em.merge(domain);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = domain.getId();
                if (findDomain(id) == null) {
                    throw new EntityNotFoundException("The domain with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }

	}

	@Override
	public Domain findDomain(int id) {
		EntityManager em = getEntityManager();
        try {
            return em.find(Domain.class, id);
        } finally {
            em.close();
        }
	}

	@Override
	public List<Domain> findDomainEntities() {
		return findDomainEntities(true, -1, -1);
	}

	@Override
	public List<Domain> findDomainEntities(int maxResults, int firstResult) {
		return findDomainEntities(false, maxResults, firstResult);
	}
	
	private List<Domain> findDomainEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Domain.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

	@Override
	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	@Override
	public int getDomainCount() {
		EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Domain> rt = cq.from(Domain.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
	}

}
