package com.ricardo.domain_crawler.service;


import com.ricardo.domain_crawler.dao.PageDAO;
import com.ricardo.domain_crawler.dao.PageDAOImpl;
import com.ricardo.domain_crawler.entity.Page;
import com.ricardo.domain_crawler.utils.SessionFactoryUtil;

public class PageService {
	//ATRIBUTOS
	private PageDAO pageDAO;
	
	//CONSTRUTORES
	public PageService() {
		pageDAO = new PageDAOImpl(SessionFactoryUtil.getSessionFactory());
	}
	
	//METODOS
	public void create(Page page) {
		pageDAO.create(page);
	}
}
