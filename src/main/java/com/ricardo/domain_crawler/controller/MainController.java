package com.ricardo.domain_crawler.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

import com.ricardo.domain_crawler.entity.Domain;
import com.ricardo.domain_crawler.entity.Page;
import com.ricardo.domain_crawler.service.PageService;

import javafx.fxml.Initializable;

public class MainController implements Initializable{
	//ATRIBUTOS
	private PageService pageService;
	
	
	public MainController() {
		pageService = new PageService();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			pageService.create(new Page("TEST", "", "", new URL("https://www.google.pt"), new Domain("google.pt")));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
