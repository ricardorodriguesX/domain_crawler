package com.ricardo.domain_crawler.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.google.common.net.InternetDomainName;

public class DownloaderConsumer implements Runnable{
	// ATRIBUTOS
	private BlockingQueue<URL> queue;
	private BlockingQueue<URL> controlQueue;
	private boolean state = false;


	// ACESSORES
	public BlockingQueue<URL> getQueue() {
		return queue;
	}

	public void setQueue(BlockingQueue<URL> queue) {
		this.queue = queue;
	}

	// CONSTRUTORES
	public DownloaderConsumer() {
		queue = new LinkedBlockingQueue<URL>();
		controlQueue = new LinkedBlockingQueue<URL>();
	}

	public DownloaderConsumer(BlockingQueue<URL> queue) throws MalformedURLException {
		this.queue = queue;
		controlQueue = new LinkedBlockingQueue<URL>();
		this.cloneQueue();
	}

	public DownloaderConsumer(DownloaderConsumer downloaderConsumer)
			throws CloneNotSupportedException, MalformedURLException {
		this(downloaderConsumer.clone().getQueue());
	}

	// METODOS
	private void process(URL url) {
		try {
			System.out.println(url);
			Downloader downloader = new Downloader(url);

			downloader.download();

			for (URL urlDownloader : downloader.getLinks()) {

				InternetDomainName name = InternetDomainName.from(urlDownloader.getHost()).topPrivateDomain();

				if (this.checkDomainExists(name.toString()) && !this.checkLinkExists(urlDownloader)) {
					queue.add(urlDownloader);
				}
			}
		} catch (Exception e) {
		}
	}
	
	@Override
	public void run() {
		state = true;
		try {
            while (state) {
                URL take = queue.take();
                process(take);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
	}
	
	public void stop() {
		this.state = false;
	}
	
	private void cloneQueue() throws MalformedURLException {
		for (URL url : queue) {
			controlQueue.add(new URL(url.toString()));
		}
	}

	private boolean checkDomainExists(String domain) {
		boolean exists = false;

		for (URL url : controlQueue) {
			InternetDomainName name = InternetDomainName.from(url.getHost()).topPrivateDomain();
			if (name.toString().equals(domain)) {
				exists = true;
				break;
			}
		}

		return exists;
	}

	private boolean checkLinkExists(URL link) {
		boolean exists = false;

		for (URL url : queue) {
			if (url.toString().equals(link.toString())) {
				exists = true;
				break;
			}
		}

		return exists;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((queue == null) ? 0 : queue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DownloaderConsumer other = (DownloaderConsumer) obj;
		if (queue == null) {
			if (other.queue != null)
				return false;
		} else if (!queue.equals(other.queue))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DownloaderConsumer [queue=" + queue + ", getQueue()=" + getQueue() + ", hashCode()=" + hashCode()
				+ ", getClass()=" + getClass() + ", toString()=" + super.toString() + "]";
	}

	@Override
	public DownloaderConsumer clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		try {
			return new DownloaderConsumer(this);
		} catch (Exception e) {
			return new DownloaderConsumer();
		}
	}
}
