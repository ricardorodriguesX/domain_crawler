package com.ricardo.domain_crawler.utils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

public class Downloader {
	// ATRIBUTOS
	private URL url;
	private Connection connection;
	private String html;

	// ACESSORES
	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public String getHtml() {
		return html;
	}

	// CONSTRUTORES
	public Downloader() {
		this.url = null;
	}

	public Downloader(URL url) {
		super();
		this.url = url;
	}

	public Downloader(Downloader downloader) throws CloneNotSupportedException {
		this(downloader.clone().getUrl());
	}

	// METODOS
	public void download() {
		try {
			this.connection = Jsoup.connect(url.toString())
					.userAgent(
							"Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
					.referrer("http://www.google.com");
			this.html = this.constructHtml();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String getHtmlHash() throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		byte[] byteArray = md.digest(html.getBytes(StandardCharsets.UTF_8));

		StringBuffer stringBuffer = new StringBuffer();

		for (byte b : byteArray) {
			stringBuffer.append(Integer.toHexString(0xFF & b));
		}

		return stringBuffer.toString();
	}

	public List<URL> getLinks() {
		try {
			Elements links = connection.get().select("a[href]");

			ArrayList<URL> urls = new ArrayList<URL>();

			for (Element link : links) {
				if (this.isValidURL(link.attr("href"))) {

					urls.add(new URL(link.attr("href")));

				}
			}
			
			return urls;
		} catch (Exception e) {
			return new ArrayList<>();
		}

		
	}

	private boolean isValidURL(String url) {
        try { 
            new URL(url).toURI(); 
            return true; 
        }catch (Exception e) { 
            return false; 
        } 
	}

	public HashMap<String, String> getMetadata() throws IOException {
		HashMap<String, String> metadata = new HashMap<String, String>();

		Element description = connection.get().select("meta[name=description]").first();
		String descriptionContent = description != null ? description.attr("content") : "";

		metadata.put("title", connection.get().title());
		metadata.put("keywords", connection.get().select("meta[name=keywords]").attr("content"));
		metadata.put("description", descriptionContent);

		return metadata;
	}

	public long getSize() throws IOException, URISyntaxException {
		URLConnection urlConnection = url.openConnection();
		urlConnection.connect();

		long size = urlConnection.getContentLength();

		if (size <= 0) {
			File file = new File(url.getFile());

			size = file.length();
		}

		return size;
	}

	private String constructHtml() throws Exception {
		Document document = this.connection.get();

		return Jsoup.clean(document.toString(), Whitelist.relaxed());
	}

	@Override
	public Downloader clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return new Downloader(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Downloader other = (Downloader) obj;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "Downloader [url=" + url + ", getUrl()=" + getUrl() + ", hashCode()=" + hashCode() + ", getClass()="
				+ getClass() + ", toString()=" + super.toString() + "]";
	}
}
