package com.ricardo.domain_crawler.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "domain")
public class Domain {
	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private int id;
	private String name;
	@OneToMany(targetEntity = Page.class, mappedBy = "domain", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Page> pages;
	
	//ACESSORES
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	//CONSTRUTORES
	public Domain() {
		
	}
	
	public Domain(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Domain(String name) {
		super();
		this.name = name;
	}
	
	public Domain(Domain domain) {
		this(domain.getId(), domain.getName());
	}
}
