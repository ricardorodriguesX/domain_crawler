package com.ricardo.domain_crawler.entity;

import java.net.URL;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "page")
public class Page {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String title;
	private String keywords;
	private String description;
	private URL url;
	@ManyToOne(cascade = {CascadeType.ALL})
	private Domain domain;
	
	//ACESSORES
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public URL getUrl() {
		return url;
	}
	public void setUrl(URL url) {
		this.url = url;
	}
	public Domain getDomain() {
		return domain;
	}
	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	
	//CONSTRUTORES
	public Page() {
		
	}
	
	public Page(int id, String title, String keywords, String description, URL url, Domain domain) {
		super();
		this.id = id;
		this.title = title;
		this.keywords = keywords;
		this.description = description;
		this.url = url;
		this.domain = domain;
	}
	
	public Page(String title, String keywords, String description, URL url, Domain domain) {
		super();
		this.title = title;
		this.keywords = keywords;
		this.description = description;
		this.url = url;
		this.domain = domain;
	}
	
	public Page(Page page) {
		this(page.getId(), page.getTitle(), page.getKeywords(), page.getDescription(), page.getUrl(), page.getDomain());
	}
}
