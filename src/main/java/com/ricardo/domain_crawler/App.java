package com.ricardo.domain_crawler;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


/**
 * JavaFX App
 */
public class App extends Application {

	private static Scene scene;

	@Override
	public void start(Stage stage) throws IOException {
	
		 scene = new Scene(loadFXML("view/main")); stage.setScene(scene); stage.show();
		 

	}

	static void setRoot(String fxml) throws IOException {
		scene.setRoot(loadFXML(fxml));
	}

	private static Parent loadFXML(String fxml) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
		return fxmlLoader.load();
	}

	public static void main(String[] args) throws Exception {
		/*BlockingQueue<URL> linkedList = new LinkedBlockingQueue<URL>();
		
		linkedList.add(new URL("https://wikipedia.org/"));
		
		DownloaderConsumer downloaderConsumer = new DownloaderConsumer(linkedList);
		
		for(int i = 0; i < 100; i++) {
			new Thread(downloaderConsumer).start();
		}*/
		
		launch(args);
	}

}